<footer class="content-info bg-primary w-full h-full ">
  <div class="container">
    @php(dynamic_sidebar('sidebar-footer'))
  </div>
</footer>
