<header class="banner bg-primary text-white flex ">
  <div class="container flex flex-col justify-center items-center sm:flex-row sm:justify-between">
    <a class="brand" href="{{ home_url('/') }}">
      {!! $siteName !!}
    </a>

    @if (has_nav_menu('primary_navigation'))
      <nav class="nav-primary" aria-label="{{ wp_get_nav_menu_name('primary_navigation') }}">
        {!! wp_nav_menu([
            'theme_location' => 'primary_navigation',
            'menu_class' => 'nav flex flex-col justify-center items-center sm:flex-row',
             'li_class' => 'bg-red-700 p-2 ',
            'echo' => false
            ])  !!}
      </nav>
    @endif
  {{--  {!! wp_nav_menu([
              'theme_location' => 'primary_navigation',
              'menu_class' => 'nav',
              'item_class' => 'relative text-2xl before:block before:content-[""] before:absolute before:-left-2 before:top-[50%] before:-translate-x-full before:-translate-y-[50%] before:w-16 before:h-1 before:bg-primary before:opacity-0 hover:before:opacity-100 before:transition-all before:duration:300',
              'current_item_class' => 'before:opacity-100 before:shadow-glow-primary',
              'echo' => false
          ]) !!}--}}

  </div>
</header>
