@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
  @includeFirst(['partials.content-single-' . get_post_type(), 'partials.content-single'])
  @endwhile
  <div class="app">
      <h1>{{ $title }}</h1>
      <h1>{{get_field('sub_title')}}</h1>
  </div>
@endsection
