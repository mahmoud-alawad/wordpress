<?php
/*
 * Copyright (c)
 * @author Mahmoud Alawad <awad25.ma@gmail.com/>
 */

namespace App\View\Components;

use Roots\Acorn\View\Component;

class Card extends Component
{
    public string $title;


    public function __construct(string $title)
    {
        $this->title = $title ?? "";

    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render(): \Illuminate\View\View
    {
        return $this->view('components.card');
    }
}


