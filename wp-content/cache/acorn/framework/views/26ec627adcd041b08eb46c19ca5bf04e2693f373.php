<?php $__env->startSection('content'); ?>
  <?php while(have_posts()): ?> <?php (the_post()); ?>
  <?php echo $__env->first(['partials.content-single-' . get_post_type(), 'partials.content-single'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php endwhile; ?>
  <div class="app">
      <h1><?php echo e($title); ?></h1>
      <h1><?php echo e(get_field('sub_title')); ?></h1>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/mahmoud/Local Sites/mahmoudalawad/app/public/wp-content/themes/sage/resources/views/front-page.blade.php ENDPATH**/ ?>