<header class="banner bg-primary text-white flex flex-col justify-center items-center sm:flex-row sm:justify-center">
  <a class="brand" href="<?php echo e(home_url('/')); ?>">
    <?php echo $siteName; ?>

  </a>

  <?php if(has_nav_menu('primary_navigation')): ?>
    <nav class="nav-primary container self-center w-full items-center flex sm:justify-between" aria-label="<?php echo e(wp_get_nav_menu_name('primary_navigation')); ?>">
      <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav flex flex-col justify-center items-center sm:flex-row', 'echo' => false]); ?>

    </nav>
  <?php endif; ?>
</header>
<?php /**PATH /home/mahmoud/Local Sites/mahmoudalawad/app/public/wp-content/themes/sage/resources/views/sections/header.blade.php ENDPATH**/ ?>